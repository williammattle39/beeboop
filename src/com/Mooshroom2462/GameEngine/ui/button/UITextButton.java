package com.Mooshroom2462.GameEngine.ui.button;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;

import com.Mooshroom2462.GameEngine.GameEngine;
import com.Mooshroom2462.GameEngine.texture.TextureUtils;
import com.Mooshroom2462.GameEngine.utils.StringConverter;

public class UITextButton extends UIButton{
	
	
	protected static final StringConverter sc = new StringConverter(TextureUtils.getImage("/chars.png"), new Dimension(4, 5));
	protected BufferedImage text_img;
	protected Dimension border;
	protected int scalar;

	public UITextButton(Point _point, String _text, Dimension _border, int _scalar) {
		super(_point, new Dimension((sc.convertString(_text).getWidth()*_scalar)+(_border.width*2), (sc.convertString(_text).getHeight()*_scalar)+(_border.height*2)));
		text_img = sc.convertString(_text);
		border = _border;
		scalar = _scalar;
	}
	
	public void render(GameEngine ge, Graphics g) {
		if(visible) {
			if(hovering) {
				hoverRender(ge, g);
			}else {
				prehoverRender(ge, g);
			}
			g.drawImage(text_img, point.x + border.width, point.y + border.height, text_img.getWidth()*scalar, text_img.getHeight()*scalar, null);
		}
	}

	public void prehoverRender(GameEngine ge, Graphics g) {
		
	}

	@Override
	public void hoverRender(GameEngine ge, Graphics g) {
		
	}

	@Override
	public void action(GameEngine ge, MouseEvent e) {
		
	}
}
