package com.Mooshroom2462.GameEngine.ui.button;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.event.MouseEvent;

import com.Mooshroom2462.GameEngine.GameEngine;
import com.Mooshroom2462.GameEngine.ui.GameUI;

public abstract class UIButton extends GameUI{
	
	protected boolean hovering = false;

	public UIButton(Point _point, Dimension _size) {
		super(_point, _size);
	}
	
	public void onMouseMove(GameEngine ge, MouseEvent e) {
		if(visible) {
			if(withinBounds(e)) {
				hovering = true;
			}else {
				hovering = false;
			}
		}
	}

	public void tick(GameEngine ge) {
	}

	public void render(GameEngine ge, Graphics g) {
		if(visible) {
			if(hovering) {
				hoverRender(ge, g);
			}else {
				prehoverRender(ge, g);
			}
		}
	}
	
	public void onMousePress(GameEngine ge, MouseEvent e) {
		if(hovering && e.getButton() == MouseEvent.BUTTON1) {
			action(ge, e);
		}
	}
	
	public abstract void prehoverRender(GameEngine ge, Graphics g);
	public abstract void hoverRender(GameEngine ge, Graphics g);
	public abstract void action(GameEngine ge, MouseEvent e);
}
