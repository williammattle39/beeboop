package com.Mooshroom2462.GameEngine.ui;

public class UIHandler {
	private GameUI[] uis;
	public UIHandler() {
		uis = new GameUI[0];
	}
	public UIHandler(GameUI[] gui) {
		uis = gui.clone();
	}
	public void addUI(GameUI gui) {
		GameUI[] temp_uis = uis.clone();
		uis = new GameUI[temp_uis.length + 1];
		for(int x=0;x<temp_uis.length;x++) {
			uis[x] = temp_uis[x];
		}
		uis[temp_uis.length] = gui;
	}
	public void removeUI(int index) {
		UIHandler uih = new UIHandler(getUIs().clone());
		for(int x=0;x<uis.length;x++) {
			if(x != index) {
				uih.addUI(uis[x]);
			}
		}
		uis = uih.getUIs();
	}
	public GameUI getUI(int index) {
		return uis[index];
	}
	public GameUI[] getUIs() {
		return uis;
	}
}
