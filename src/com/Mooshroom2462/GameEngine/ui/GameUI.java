package com.Mooshroom2462.GameEngine.ui;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseWheelEvent;

import com.Mooshroom2462.GameEngine.GameEngine;

public abstract class GameUI {
	protected boolean visible = false;
	protected Point point;
	protected Dimension size;
	public GameUI(Point _point, Dimension _size) {
		point = _point;
		size = _size;
	}
	public void onMousePress(GameEngine ge, MouseEvent e) {}
	public void onMouseRelease(GameEngine ge, MouseEvent e) {}
	public void onMouseWheelMove(GameEngine ge, MouseWheelEvent e) {}
	public void onMouseMove(GameEngine ge, MouseEvent e) {}
	public void onMouseDrag(GameEngine ge, MouseEvent e) {}
	public void onKeyPress(GameEngine ge, KeyEvent e) {}
	public void onKeyRelease(GameEngine ge, KeyEvent e) {}
	protected boolean withinBounds(MouseEvent e) {
		if(point.x <= e.getX() && e.getX() <= (point.x + size.width) && point.y <= e.getY() && e.getY() <= (point.y + size.height)) {
			return true;
		}else {
			return false;
		}
	}
	public void setVisible(boolean _vis) {
		visible = _vis;
	}
	public boolean isVisible() {
		return visible;
	}
	public Point getPoint() {
		return point;
	}
	public Dimension getSize() {
		return size;
	}
	public void setPoint(int _x, int _y) {
		point = new Point(_x, _y);
	}
	public void setSize(int _width, int _height) {
		size = new Dimension(_width, _height);
	}
	public abstract void tick(GameEngine ge);
	public abstract void render(GameEngine ge, Graphics g);
}
