package com.Mooshroom2462.GameEngine.ui;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.image.BufferedImage;

import com.Mooshroom2462.GameEngine.GameEngine;
import com.Mooshroom2462.GameEngine.utils.StringConverter;

public class UIText extends GameUI{
	
	private BufferedImage img;
	private int scalar;
	
	public UIText(String _text, Point _point, int _scalar, StringConverter _sc) {
		super(_point, new Dimension(0,0));
		img = _sc.convertString(_text);
		scalar = _scalar;
		size = new Dimension(img.getWidth(), img.getHeight());
	}

	public void tick(GameEngine ge) {}

	public void render(GameEngine ge, Graphics g) {
		g.drawImage(img, point.x, point.y, size.width * scalar, img.getHeight() * scalar, null);
	}
	
	public Dimension getScaledSize() {
		return new Dimension(size.width * scalar, size.height * scalar);
	}
}
