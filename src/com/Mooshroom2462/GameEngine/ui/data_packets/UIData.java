package com.Mooshroom2462.GameEngine.ui.data_packets;

import java.awt.Dimension;
import java.awt.Point;
import java.awt.image.BufferedImage;

public class UIData {
	private BufferedImage img;
	private Point point;
	private Dimension dim;
	public UIData(BufferedImage _img, Point _point, Dimension _dim) {
		img = _img;
		point = _point;
		dim = _dim;
	}
	public BufferedImage getImage() {
		return img;
	}
	public Point getPoint() {
		return point;
	}
	public Dimension getDimension() {
		return dim;
	}
}
