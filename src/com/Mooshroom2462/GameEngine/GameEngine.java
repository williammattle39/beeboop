package com.Mooshroom2462.GameEngine;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;

import javax.swing.JFrame;
import javax.swing.JPanel;

import com.Mooshroom2462.GameEngine.state.GameState;
import com.Mooshroom2462.GameEngine.state.StateHandler;

public class GameEngine extends JFrame implements Runnable{
	private static final long serialVersionUID = 1L;
	
	public static final int DYNAMIC_MODE = 1;
	public static final int STATIC_MODE = 0;
	
	private Dimension Dynamic, Static;
	
	private JPanel renderer;
	private Dimension rendererSize;
	
	private int FPS;
	private boolean running = false;
	private Thread instance;
	
	private int elapsedFPS = 0;
	
	private StateHandler states;
	private int current_state = 0;
	
	public GameEngine(String _title, int _FPS, Dimension _size, GameState[] _states) {
		FPS = _FPS;
		states = new StateHandler(_states);
		rendererSize = _size;
		generateModes();
		windowSetup(_title);
	}
	
	public GameEngine(String _title, int _FPS, Dimension _size, StateHandler _sh) {
		FPS = _FPS;
		states = _sh;
		rendererSize = _size;
		generateModes();
		windowSetup(_title);
	}
	
	private void generateModes() {
		JFrame StaticFrame = new JFrame();
		JFrame DynamicFrame = new JFrame();
		JPanel StaticFramePanel = new JPanel();
		JPanel DynamicFramePanel = new JPanel();
		StaticFrame.setResizable(false);
		DynamicFrame.setResizable(true);
		StaticFrame.add(StaticFramePanel);
		DynamicFrame.add(DynamicFramePanel);
		StaticFrame.setVisible(true);
		DynamicFrame.setVisible(true);
		Static = new Dimension(StaticFrame.getWidth() - StaticFramePanel.getWidth(), StaticFrame.getHeight() - StaticFramePanel.getHeight());
		Dynamic = new Dimension(DynamicFrame.getWidth() - DynamicFramePanel.getWidth(), DynamicFrame.getHeight() - DynamicFramePanel.getHeight());
		StaticFrame.dispose();
		DynamicFrame.dispose();
	}
	
	private void windowSetup(String _title) {
		setTitle(_title);
		GameEngine ge = this;
		renderer = new JPanel() {
			private static final long serialVersionUID = 1L;
			public void paint(Graphics g) {
				g.clearRect(0, 0, this.getWidth(), this.getHeight());
				states.getState(current_state).render(ge, g);
				states.getState(current_state).UIRender(ge, g);
				g.dispose();
			}
		};
		renderer.addMouseListener(new MouseListener() {
			public void mousePressed(MouseEvent e) {
				states.getState(current_state).UIOnMousePress(ge, e);
				states.getState(current_state).onMousePress(ge, e);
			}
			public void mouseReleased(MouseEvent e) {
				states.getState(current_state).UIOnMouseRelease(ge, e);
				states.getState(current_state).onMouseRelease(ge, e);
			}
			public void mouseClicked(MouseEvent e) {}
			public void mouseEntered(MouseEvent e) {}
			public void mouseExited(MouseEvent e) {}
		});
		renderer.addMouseMotionListener(new MouseMotionListener() {
			public void mouseDragged(MouseEvent e) {
				states.getState(current_state).UIOnMouseDrag(ge, e);
				states.getState(current_state).onMouseDrag(ge, e);
			}
			public void mouseMoved(MouseEvent e) {
				states.getState(current_state).UIOnMouseMove(ge, e);
				states.getState(current_state).onMouseMove(ge, e);
			}
		});
		renderer.addMouseWheelListener(new MouseWheelListener() {
			public void mouseWheelMoved(MouseWheelEvent e) {
				states.getState(current_state).UIOnMouseWheelMove(ge, e);
				states.getState(current_state).onMouseWheelMove(ge, e);
			}
		});
		add(renderer);
		addKeyListener(new KeyListener() {
			public void keyPressed(KeyEvent e) {
				states.getState(current_state).UIOnKeyPress(ge, e);
				states.getState(current_state).onKeyPress(ge, e);
			}
			public void keyReleased(KeyEvent e) {
				states.getState(current_state).UIOnKeyRelease(ge, e);
				states.getState(current_state).onKeyRelease(ge, e);
			}
			public void keyTyped(KeyEvent e) {}
		});
		setResizeMode(STATIC_MODE);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	
	public synchronized void setResizeMode(int _mode) {
		if(_mode == 0) {
			setResizable(false);
			setSize(rendererSize.width + Static.width, rendererSize.height + Static.height);
		}else {
			setResizable(true);
			setSize(rendererSize.width + Dynamic.width, rendererSize.height + Dynamic.height);
		}
	}
	
	public synchronized void setState(int index) {
		current_state = index;
	}
	
	public synchronized void start() {
		running = true;
		instance = new Thread(this);
		setVisible(true);
		instance.start();
	}
	
	public synchronized void stop() {
		setVisible(false);
		dispose();
		running = false;
	}
	
	public void run() {
		long startTime, endTime, delta, elapsedStartTime;
		int frames = 0;
		elapsedStartTime = System.currentTimeMillis();
		while(running) {
			startTime = System.currentTimeMillis(); 
			if(states.getState(current_state).initialized == false) {
				states.getState(current_state).init(this);
				states.getState(current_state).initialized = true;
			}
			states.getState(current_state).tick(this);
			renderer.repaint();
			endTime = System.currentTimeMillis();
			delta = endTime - startTime;
			if ((endTime - elapsedStartTime) >= 1000) {
				elapsedFPS = frames;
				elapsedStartTime = endTime;
				frames = 0;
			}else {
				frames++;
			}
			if((1000/FPS) - delta > 0) {
				try {
					Thread.sleep((1000/FPS) - delta);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	public int getElapsedFPS() {
		return elapsedFPS;
	}
	
	public JPanel getRenderer() {
		return renderer;
	}
}
