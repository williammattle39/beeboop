package com.Mooshroom2462.GameEngine.utils;

import java.io.InputStream;

public class Utilities {
	public static final InputStream getResourceStream(String _path) {
		return Thread.currentThread().getClass().getResourceAsStream(_path);
	}
}
