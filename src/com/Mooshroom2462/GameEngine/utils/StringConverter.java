package com.Mooshroom2462.GameEngine.utils;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.image.BufferedImage;

public class StringConverter {
	
	private static final String[] CHARS = "abcdefghijklmnopqrstuvwxyz0123456789.!?,;:".split("");
	private BufferedImage[] img_chars;
	private Dimension char_size;
	
	public StringConverter(BufferedImage _img, Dimension _char_size) {
		char_size = _char_size;
		img_chars = new BufferedImage[CHARS.length];
		for(int x=0;x<img_chars.length;x++) {
			img_chars[x] = _img.getSubimage(x*char_size.width, 0, char_size.width, char_size.height);
		}
	}
	
	public BufferedImage convertString(String _text) {
		String[] letters = _text.split("");
		BufferedImage img = new BufferedImage(letters.length*char_size.width, char_size.height, BufferedImage.TYPE_INT_ARGB);
		Graphics g = img.createGraphics();
		for(int x=0;x<letters.length;x++) {
			if(getCharIndex(letters[x]) != -1) {
				g.drawImage(img_chars[getCharIndex(letters[x])], x*char_size.width, 0, char_size.width, char_size.height, null);
			}
		}
		return img;
	}
	
	private int getCharIndex(String _char) {
		int index = -1;
		for(int x=0;x<CHARS.length;x++) {
			if((CHARS[x].toLowerCase()).equals(_char.toLowerCase())) {
				index = x;
			}
		}
		return index;
	}
}
