package com.Mooshroom2462.GameEngine.texture;

import java.awt.image.BufferedImage;
import java.io.IOException;
import javax.imageio.ImageIO;
import com.Mooshroom2462.GameEngine.utils.Utilities;

public class TextureUtils {
	public static final BufferedImage getImage(String _path) {
		BufferedImage img;
		try {
			img = ImageIO.read(Utilities.getResourceStream(_path));
		} catch (IOException e) {
			img = null;
		}
		return img;
	}
}
