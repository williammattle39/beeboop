package com.Mooshroom2462.GameEngine.state;

import java.awt.Graphics;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseWheelEvent;

import com.Mooshroom2462.GameEngine.GameEngine;
import com.Mooshroom2462.GameEngine.ui.UIHandler;

public abstract class GameState {
	public boolean initialized = false;
	protected UIHandler uis = new UIHandler();
	public void UIOnMousePress(GameEngine ge, MouseEvent e) {
		for(int x=0;x<uis.getUIs().length;x++) {
			uis.getUIs()[x].onMousePress(ge, e);
		}
	}
	public void UIOnMouseRelease(GameEngine ge, MouseEvent e) {
		for(int x=0;x<uis.getUIs().length;x++) {
			uis.getUIs()[x].onMouseRelease(ge, e);
		}
	}
	public void UIOnMouseWheelMove(GameEngine ge, MouseWheelEvent e) {
		for(int x=0;x<uis.getUIs().length;x++) {
			uis.getUIs()[x].onMouseWheelMove(ge, e);
		}
	}
	public void UIOnMouseMove(GameEngine ge, MouseEvent e) {
		for(int x=0;x<uis.getUIs().length;x++) {
			uis.getUIs()[x].onMouseMove(ge, e);
		}
	}
	public void UIOnMouseDrag(GameEngine ge, MouseEvent e) {
		for(int x=0;x<uis.getUIs().length;x++) {
			uis.getUIs()[x].onMouseDrag(ge, e);
		}
	}
	public void UIOnKeyPress(GameEngine ge, KeyEvent e) {
		for(int x=0;x<uis.getUIs().length;x++) {
			uis.getUIs()[x].onKeyPress(ge, e);
		}
	}
	public void UIOnKeyRelease(GameEngine ge, KeyEvent e) {
		for(int x=0;x<uis.getUIs().length;x++) {
			uis.getUIs()[x].onKeyRelease(ge, e);
		}
	}
	public void UITick(GameEngine ge) {
		for(int x=0;x<uis.getUIs().length;x++) {
			uis.getUIs()[x].tick(ge);
		}
	}
	public void UIRender(GameEngine ge, Graphics g) {
		for(int x=0;x<uis.getUIs().length;x++) {
			uis.getUIs()[x].render(ge, g);
		}
	}
	public UIHandler getUIHandler() {
		return uis;
	}
	public void onMousePress(GameEngine ge, MouseEvent e) {}
	public void onMouseRelease(GameEngine ge, MouseEvent e) {}
	public void onMouseWheelMove(GameEngine ge, MouseWheelEvent e) {}
	public void onMouseMove(GameEngine ge, MouseEvent e) {}
	public void onMouseDrag(GameEngine ge, MouseEvent e) {}
	public void onKeyPress(GameEngine ge, KeyEvent e) {}
	public void onKeyRelease(GameEngine ge, KeyEvent e) {}
	public abstract void init(GameEngine ge);
	public abstract void tick(GameEngine ge);
	public abstract void render(GameEngine ge, Graphics g);
}
