package com.Mooshroom2462.GameEngine.state;

public class StateHandler {
	private GameState[] states;
	public StateHandler() {
		states = new GameState[0];
	}
	public StateHandler(GameState[] gs) {
		states = gs.clone();
	}
	public void addState(GameState gs) {
		GameState[] temp_states = states.clone();
		states = new GameState[temp_states.length + 1];
		for(int x=0;x<temp_states.length;x++) {
			states[x] = temp_states[x];
		}
		states[temp_states.length] = gs;
	}
	public void removeState(int index) {
		StateHandler sh = new StateHandler(getStates().clone());
		for(int x=0;x<states.length;x++) {
			if(x != index) {
				sh.addState(states[x]);
			}
		}
		states = sh.getStates();
	}
	public GameState getState(int index) {
		return states[index];
	}
	public GameState[] getStates() {
		return states;
	}
}
