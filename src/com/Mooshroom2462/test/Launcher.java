package com.Mooshroom2462.test;
import java.awt.Dimension;
import com.Mooshroom2462.GameEngine.GameEngine;
import com.Mooshroom2462.GameEngine.state.StateHandler;
public class Launcher {
	public static void main(String[] args) {
		StateHandler sh = new StateHandler();
		sh.addState(new RayState());
		GameEngine ge = new GameEngine("RayCasting", 60, new Dimension(1024,512), sh);
		ge.setResizeMode(GameEngine.STATIC_MODE);
		ge.start();
	}
}
