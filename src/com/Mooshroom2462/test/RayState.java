package com.Mooshroom2462.test;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.KeyEvent;
import java.awt.event.MouseWheelEvent;

import com.Mooshroom2462.GameEngine.GameEngine;
import com.Mooshroom2462.GameEngine.state.GameState;

public class RayState extends GameState{
	
	private int map_width = 8;
	private int map_height = 8;
	private int tilesize = 64;
	
	private float angle = 0.0f;
	private float rotation_speed = 0.05f;
	private float player_x = 72f;
	private float player_y = 72f;
	private int ray_magnitude = 16;
	private double speed = 1;
	
	private boolean move_forward = false;
	private boolean move_backward = false;
	private boolean rotate_left = false;
	private boolean rotate_right = false;
	
	private int max_iterations = 8;
	private int ray_number = 256;
	private float pov_angle = (float) (0.33333333 * Math.PI);
	
	private int screen_size = 512;
	
	private static final int[] MAP = {
			1,1,1,1,1,1,1,1,
			1,0,0,0,0,0,0,1,
			1,1,1,0,0,1,1,1,
			1,0,0,1,0,0,0,1,
			1,0,0,0,1,0,0,1,
			1,0,0,0,1,0,0,1,
			1,0,0,0,1,0,0,1,
			1,1,1,1,1,1,1,1
	};
	
	public void init(GameEngine ge) {
		
	}

	public void tick(GameEngine ge) {
		//Normal player move and collision
		int fx, fy, bx, by;
		fx = (int) ((player_x + (Math.cos(angle) * ray_magnitude)) / tilesize);
		fy = (int) ((player_y + (Math.sin(angle) * ray_magnitude)) / tilesize);
		bx = (int) ((player_x - (Math.cos(angle) * ray_magnitude)) / tilesize);
		by = (int) ((player_y - (Math.sin(angle) * ray_magnitude)) / tilesize);
		if(move_forward) {
			if (MAP[(fy * map_width) + fx] == 0) {
				player_x += (Math.cos(angle) * speed);
				player_y += (Math.sin(angle) * speed);
			}
		}
		if(move_backward) {
			if (MAP[(by * map_width) + bx] == 0) {
				player_x -= (Math.cos(angle) * speed);
				player_y -= (Math.sin(angle) * speed);
			}
		}
		if(rotate_right) {
			angle += rotation_speed;
		}
		if(rotate_left) {
			angle -= rotation_speed;
		}
		if(angle < 0) {
			angle += (2 * Math.PI);
		}else if(angle > (2 * Math.PI)) {
			angle -= (2 * Math.PI);
		}
		angle *= 100;
		angle = Math.round(angle);
		angle /= 100;
	}
	
	@Override
	public void onKeyPress(GameEngine ge, KeyEvent e) {
		switch (e.getKeyCode()){
			case KeyEvent.VK_W:
				move_forward = true;
				break;
			case KeyEvent.VK_A:
				rotate_left = true;
				break;
			case KeyEvent.VK_S:
				move_backward = true;
				break;
			case KeyEvent.VK_D:
				rotate_right = true;
				break;
			case KeyEvent.VK_SHIFT:
				speed = 1.8;
				break;
		}
	}
	
	@Override
	public void onKeyRelease(GameEngine ge, KeyEvent e) {
		switch (e.getKeyCode()){
		case KeyEvent.VK_W:
			move_forward = false;
			break;
		case KeyEvent.VK_A:
			rotate_left = false;
			break;
		case KeyEvent.VK_S:
			move_backward = false;
			break;
		case KeyEvent.VK_D:
			rotate_right = false;
			break;
		case KeyEvent.VK_SPACE:
			pov_angle = (float) (0.125 * Math.PI);
			break;
		case KeyEvent.VK_SHIFT:
			speed = 1;
			break;
		}
		System.out.println(angle);
	}
	
	public void onMouseWheelMove(GameEngine ge, MouseWheelEvent e) {
		pov_angle += e.getWheelRotation() * 0.01;
	}

	public void render(GameEngine ge, Graphics g) {
		g.setColor(Color.cyan);
		g.fillRect(0, 0, ge.getRenderer().getSize().width, ge.getRenderer().getSize().height);
		for(int y=0;y<map_height;y++) {
			for(int x=0;x<map_width;x++) {
				if(MAP[(y * map_width) + x] == 0) {
					g.setColor(Color.WHITE);
				}else {
					g.setColor(Color.BLACK);
				}
				g.fillRect(x * tilesize, y * tilesize, tilesize, tilesize);
			}
		}
		g.setColor(Color.BLACK);
		g.drawLine((int) player_x, (int) player_y, (int) (player_x + (Math.cos(angle) * ray_magnitude)), (int) (player_y + (Math.sin(angle) * ray_magnitude)));
		g.setColor(Color.WHITE);
		g.drawString("FPS: " + ge.getElapsedFPS(), 0, 12);
		//
		//RayCasting
		//
		g.setColor(Color.GREEN);
		float ray_angle = (float) (angle - (pov_angle * 0.5));
		float ray_increment = pov_angle / (ray_number - 1);
		float line_width = (screen_size / ray_number);
		double rounded_x, rounded_y, rounded_x_1, rounded_y_1, distance, distance_1, use_distance, yo, xo, atan, ntan;
		int dof, mx, my, mp;
		for (int i = 0;i < ray_number;i++) {
			if(ray_angle < 0) {
				ray_angle += (2 * Math.PI);
			}else if(ray_angle > (2 * Math.PI)) {
				ray_angle -= (2 * Math.PI);
			}
			dof = 0;
			atan = (float) (-1 / Math.tan(ray_angle));
			ntan = (float) (-Math.tan(ray_angle));
			rounded_y = player_y;
			rounded_x = player_x;
			rounded_y_1 = player_y;
			rounded_x_1 = player_x;
			yo = 0;
			xo = 0;
			distance = Integer.MAX_VALUE;
			distance_1 = Integer.MAX_VALUE;
			if(ray_angle > Math.PI) {
				rounded_y = (((int) (player_y / tilesize)) * tilesize) - 0.0001;
				rounded_x = (atan * (player_y - rounded_y)) + player_x;
				yo = -64;
				xo = -yo * atan;
			}
			if(ray_angle < Math.PI) {
				rounded_y = (((int) (player_y / tilesize)) * tilesize) + tilesize;
				rounded_x = (atan * (player_y - rounded_y)) + player_x;
				yo = 64;
				xo = -yo * atan;
			}
			if(ray_angle == 0 || ray_angle == Math.PI){
				dof = max_iterations;
				rounded_y = player_y;
				rounded_x = player_x;
			}
			while(dof < max_iterations) {
				mx = (int) (rounded_x / tilesize);
				my = (int) (rounded_y / tilesize);
				mp = (my * map_width) + mx;
				if(mp >= 0 && mp < MAP.length) {
					if(MAP[mp] == 1) {
						dof = max_iterations;
					}else {
						rounded_x += xo;
						rounded_y += yo;
					}
				}
				dof += 1;
			}
			
			distance = pythagorean(player_x, player_y, (float) rounded_x, (float) rounded_y);
			
			dof = 0;
			yo = 0;
			xo = 0;
			if(ray_angle > 0.5 * Math.PI && ray_angle < 1.5 * Math.PI) {
				rounded_x_1 = (((int) (player_x / tilesize)) * tilesize) - 0.0001;
				rounded_y_1 = (ntan * (player_x - rounded_x_1)) + player_y;
				xo = -64;
				yo = -xo * ntan;
			}
			if(ray_angle < 0.5 * Math.PI || ray_angle > 1.5 * Math.PI) {
				rounded_x_1 = (((int) (player_x / tilesize)) * tilesize) + tilesize;
				rounded_y_1 = (ntan * (player_x - rounded_x_1)) + player_y;
				xo = 64;
				yo = -xo * ntan;
			}
			if(ray_angle == 0 || ray_angle == Math.PI){
				dof = max_iterations;
				rounded_y_1 = player_y;
				rounded_x_1 = player_x;
			}
			while(dof < max_iterations) {
				mx = (int) (rounded_x_1 / tilesize);
				my = (int) (rounded_y_1 / tilesize);
				mp = (my * map_width) + mx;
				if(mp >= 0 && mp < MAP.length) {
					if(MAP[mp] == 1) {
						dof = max_iterations;
					}else {
						rounded_x_1 += xo;
						rounded_y_1 += yo;
					}
				}
				dof += 1;
			}
			
			int color_offset = 0;
			
			distance_1 = pythagorean(player_x, player_y, (float) rounded_x_1, (float) rounded_y_1);
			
			g.setColor(Color.GREEN);
			if(distance_1 > distance) {
				use_distance = distance;
				g.drawLine((int) player_x, (int) player_y, (int) rounded_x, (int) rounded_y);
				color_offset = 0;
			}else {
				use_distance = distance_1;
				g.drawLine((int) player_x, (int) player_y, (int) rounded_x_1, (int) rounded_y_1);
				color_offset = 30;
			}
			float ca, unwarped_distance;
			ca = angle - ray_angle;
			if(ca < 0) {
				ca += (2 * Math.PI);
			}else if(ca > (2 * Math.PI)) {
				ca -= (2 * Math.PI);
			}
			unwarped_distance = (float) (use_distance * Math.cos(ca));
			int height = (int) ((screen_size * tilesize) / unwarped_distance);
			if (height > screen_size) {
				height = screen_size;
			}
			int rect_y = (screen_size / 2) - (height / 2);
			g.setColor(new Color(255 - color_offset, 197 - color_offset, 39));
			g.fillRect((int)(screen_size + (i * line_width)), rect_y, (int) (line_width), height);
			g.setColor(Color.BLACK);
			g.drawRect(screen_size, 0, screen_size, screen_size);
			ray_angle += ray_increment;
		}
	}
	
	private float pythagorean(float px, float py, float px1, float py1) {
		return (float) Math.sqrt(Math.pow((px1 - px), 2) + Math.pow((py1 - py), 2));
	}
}
